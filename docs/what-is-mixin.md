# Mixin in Python

A mixin in Python is a class that provides a set of additional methods or attributes that can be "mixed in" with other classes. Mixins are typically designed to be reusable and are intended to enhance the functionality of classes without requiring inheritance from a common base class.

## Characteristics of Mixins

1. **Modularity**: Mixins promote modularity by encapsulating specific functionality in separate classes. This allows developers to compose classes by combining multiple mixins, rather than inheriting from a single, monolithic base class.

2. **Reusability**: Mixins are designed to be reusable across different classes or projects. Since mixins do not define a complete class hierarchy on their own, they can be mixed into any class that requires the additional functionality they provide.

3. **Composition over Inheritance**: Mixins follow the principle of composition over inheritance, which encourages assembling complex behaviors by composing smaller, reusable components rather than relying on deep class hierarchies.

4. **Single Responsibility Principle (SRP)**: Mixins adhere to the Single Responsibility Principle by focusing on a single aspect of functionality. Each mixin is responsible for providing a specific set of methods or attributes related to a particular concern.

5. **Mixin Naming Convention**: Mixin classes often have names ending with "Mixin" to indicate their purpose. For example, a mixin providing logging functionality may be named `LoggingMixin`.

## Example of Using Mixins

```python
class PrintableMixin:
    def print_info(self):
        print(f"Information: {self.info}")

class EmailNotifierMixin:
    def send_email(self, recipient):
        print(f"Sending email to {recipient}: {self.info}")

class Task(PrintableMixin, EmailNotifierMixin):
    def __init__(self, info):
        self.info = info

task = Task("Complete the assignment")
task.print_info()  # Output: Information: Complete the assignment
task.send_email("example@example.com")  # Output: Sending email to example@example.com: Complete the assignment
```
### Explanation
In this example, PrintableMixin provides a print_info() method for printing information, while EmailNotifierMixin provides a send_email() method for sending emails.
The Task class inherits from both mixins, allowing instances of Task to access methods from both mixins.
By using mixins, the Task class gains functionality from both PrintableMixin and EmailNotifierMixin without the need for multiple inheritance or duplicating code.

Overall, mixins offer a flexible and modular approach to extending class functionality in Python, enabling code reuse and maintainability.
