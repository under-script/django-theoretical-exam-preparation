# Django Templates

In Django, templates are text files containing HTML markup with embedded template tags and variables. Templates are used to generate dynamic content dynamically and to produce the final HTML output sent to clients (such as web browsers) in response to HTTP requests.

## Key Features of Django Templates

1. **Template Engine**: Django comes with a built-in template engine that parses template files and processes template tags and variables. The template engine allows developers to write HTML templates with dynamic content using Django's template language.

2. **Template Tags**: Django templates support template tags, which are special syntax constructs enclosed within `{% %}` delimiters. Template tags provide control flow logic (e.g., loops, conditionals), variable assignments, template inheritance, and other functionalities within templates.

3. **Template Variables**: Django templates support template variables, which are placeholders for dynamic data. Template variables are enclosed within `{{ }}` delimiters and can be replaced with actual values or objects passed from views to templates.

4. **Template Inheritance**: Django templates support template inheritance, allowing developers to create base templates with common layout and structure and extend or override specific blocks within child templates. This promotes code reuse and maintains consistency across multiple pages.

5. **Static and Dynamic Content**: Templates can contain both static content (HTML markup, text) and dynamic content (template tags, variables). Dynamic content is generated dynamically based on data provided by views or context processors.

6. **Template Loading**: Django automatically loads templates from the directories specified in the `TEMPLATES` setting in the project settings file (`settings.py`). Templates can be organized into multiple directories and apps, making it easy to manage and reuse templates across the project.

7. **Context Data**: Templates receive context data from views, which are dictionaries containing data to be used in the template rendering process. Context data can include variables, objects, and other information required for generating dynamic content in templates.

8. **Template Filters**: Django templates support template filters, which allow developers to modify or format template variables before displaying them in the template. Filters are applied using the `|` pipe symbol and provide functionalities such as date formatting, string manipulation, and more.

## Example of a Django Template

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ title }}</title>
</head>
<body>
    <h1>{{ greeting }}</h1>
    <p>Welcome to {{ website_name }}</p>
    {% if user.is_authenticated %}
        <p>Hello, {{ user.username }}!</p>
    {% else %}
        <p>Please log in to continue.</p>
    {% endif %}
</body>
</html>
```
In this example:

The template contains HTML markup with embedded template variables ({{ title }}, {{ greeting }}, {{ website_name }}, {{ user.username }}) and template tags ({% if %}).
The template engine replaces template variables with actual values provided by the view's context data.
Template tags provide conditional logic to display different content based on whether the user is authenticated or not.
Overall, Django's template system provides a powerful mechanism for generating dynamic HTML content and separating presentation logic from application logic in web applications.
