# Django URLs

In Django, URLs (Uniform Resource Locators) are used to map incoming HTTP requests to views or other endpoints within a web application. URLs define the structure of a web application's navigation and provide a mapping between URLs and view functions or class-based views.

## Key Features of Django URLs

1. **URL Patterns**: Django URLs are defined using URL patterns, which are regular expressions (regex) or strings that match specific URL patterns. URL patterns are defined in the project's URL configuration module (usually named `urls.py`).

2. **URL Configuration**: Django applications typically have a central URL configuration module (`urls.py`) where URL patterns are defined. This module contains a list of URL patterns and their corresponding view functions or class-based views.

3. **URL Dispatcher**: Django's URL dispatcher is responsible for matching incoming HTTP requests to the appropriate view based on the requested URL pattern. The URL dispatcher examines the URL path provided in the request and compares it against the defined URL patterns to determine which view to invoke.

4. **URL Naming**: Django allows developers to name URLs, which provides a convenient way to reference URLs in templates or view functions without hardcoding URLs. Named URLs are defined using the `name` parameter in the URL pattern definition.

5. **Dynamic URLs**: Django supports dynamic URLs by using URL parameters or capturing parts of the URL path. URL parameters are specified using angle brackets (`<parameter_name>`), allowing views to access and process dynamic data from the URL.

6. **Namespace and Include**: Django allows developers to organize URL patterns into namespaces and includes. Namespaces provide a way to group related URL patterns within a specific namespace, while includes allow including URL patterns from other URL configuration modules.

7. **Reverse URL Resolution**: Django provides the `reverse` function, which allows developers to generate URLs dynamically based on their named URL patterns. This allows for decoupling between URL definitions and view implementations, making URLs more maintainable and flexible.

## Example of Django URLs

```python
from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('products/<int:product_id>/', views.product_detail, name='product_detail'),
]
```
In this example:

The urlpatterns list contains three URL patterns, each mapped to a view function.
The empty string '' matches the root URL and is mapped to the home view.
The URL pattern 'about/' matches the URL /about/ and is mapped to the about view.
The URL pattern 'products/<int:product_id>/' matches URLs with a numeric product ID and is mapped to the product_detail view.
Overall, Django's URL routing system provides a powerful mechanism for defining the structure of a web application's navigation and routing incoming requests to the appropriate views or endpoints.
